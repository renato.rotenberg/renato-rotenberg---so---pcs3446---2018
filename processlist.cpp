#include "processlist.h"

ProcessList::ProcessList()
{

}

void ProcessList::addProcess(const Process &process, VM &vm)
{
    processes.push_back(process);
    if (processes.size() == 1)
        vm.restoreContext(process);
}

VM::CycleResult ProcessList::evalProcess(VM &vm)
{
    if (currentActiveCicles < maxActiveCicles)
    {
        currentActiveCicles++;
    }
    else
    {
        currentActiveCicles = 0;
        if (processes.size() > 1)
        {
            Process proc = processes.front();
            processes.pop_front();
            vm.saveContext(proc);
            processes.push_back(proc);
            vm.restoreContext(processes.front());
        }
    }

    processes.front().loadProcess(vm);
    return vm.eval();
}

void ProcessList::removeCurrentProcess(VM &vm)
{
    currentActiveCicles = 0;
    processes.front().endProcess(vm);
    processes.pop_front();
    if (!processes.empty())
        vm.restoreContext(processes.front());
}

Process &ProcessList::front()
{
    return processes.front();
}
