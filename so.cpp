#include "so.h"

#include <fstream>
#include <iostream>
#include <chrono>
#include <array>

SO::SO()
{

}

void SO::teste1()
{
    std::ifstream n2File("programs/renato/n2.txt");
    std::string exe = "";
    n2File >> exe;
    Process n2(1, exe);
    processList.addProcess(n2, vm);
    for (;;)
    {
        if (processList.evalProcess(vm) == VM::CycleResult::finished)
            break;
    }
    std::cout << "Resultado do teste 1: 4 ao quadrado igual a " << (int) vm.mem[0x6035] << std::endl;
}

void SO::teste2()
{
    std::ifstream n2File("programs/renato/work500m.txt");
    std::string exe = "";
    n2File >> exe;
    std::vector<Process> works;
    for (size_t i = 0; i < 4; i++)
    {
        works.emplace_back(i, exe);
        processList.addProcess(works[i], vm);
    }
    std::array<double, 4> duration;
    // comeco da execucao
    auto start = std::chrono::steady_clock::now();
    for (size_t i = 0;;)
    {
        if (processList.evalProcess(vm) == VM::CycleResult::finished)
        {
            auto end = std::chrono::steady_clock::now();
            auto diff = end - start;
            duration[i] = std::chrono::duration <double, std::milli> (diff).count();
            processList.removeCurrentProcess(vm);
            i++;
            if (i == duration.size())
                break;
        }
    }
    for (size_t i = 0; i < duration.size(); i++)
        std::cout << "Resultado do teste 2: Processo " << i << " acaba depois de " << duration[i] << " ms" << std::endl;
}

void SO::teste3()
{
    std::ifstream n2File("programs/renato/work500m.txt");
    std::string exe = "";
    n2File >> exe;
    std::vector<Process> works;
    for (size_t i = 0; i < 4; i++)
    {
        works.emplace_back(i, exe);
    }
    std::array<double, 4> duration;
    std::array<double, 4> start;
    std::array<double, 4> end;
    // comeco da execucao
    processList.addProcess(works[0], vm);
    start[0] = 0;
    {
        size_t j = 1;
        size_t i = 0;
        double step = 0;
        auto t0 = std::chrono::steady_clock::now();
        auto lastEndTime = std::chrono::steady_clock::now();
        for (;;)
        {
            if (processList.evalProcess(vm) == VM::CycleResult::finished)
            {
                auto id = processList.front().id;
                auto termino = std::chrono::steady_clock::now();
                end[id] = std::chrono::duration<double, std::milli>(termino - t0).count();
                duration[id] = end[id] - start[id];
                processList.removeCurrentProcess(vm);
                i++;
                if (i == 4)
                    break;
            }

            auto frameDelta = std::chrono::steady_clock::now() - lastEndTime;
            step += std::chrono::duration<double, std::milli>(frameDelta).count();
            lastEndTime = std::chrono::steady_clock::now();
            if (j < 4 && step >= 250)
            {
                processList.addProcess(works[j], vm);
                start[j] = std::chrono::duration<double, std::milli>(std::chrono::steady_clock::now() - t0).count();
                step -= 250;
                j++;
            }

        }
    }
    for (size_t i = 0; i < 4; i++)
    {
        std::cout << "Resultado do teste 3: Processo " << i << " começa no instante " << start[i] << " ms" << std::endl;
        std::cout << "                                 termina no instante " << end[i] << " ms" << std::endl;
        std::cout << "                                 durando " << duration[i] << " ms no total" << std::endl;
    }
}
