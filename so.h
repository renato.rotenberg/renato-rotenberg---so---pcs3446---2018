#ifndef SO_H
#define SO_H

#include "vm.h"
#include "processlist.h"

class SO
{
public:
    SO();

    void teste1();
    void teste2();
    void teste3();

private:
    VM vm;
    ProcessList processList;
};

#endif // SO_H
