#ifndef PROCESSLIST_H
#define PROCESSLIST_H

#include <list>

#include "process.h"
#include "vm.h"


class ProcessList
{
public:
    ProcessList();
    void addProcess(const Process &process, VM &vm);
    VM::CycleResult evalProcess(VM &vm);
    void removeCurrentProcess(VM &vm);
    Process& front();

    constexpr static int maxActiveCicles = 16;

private:
    std::list<Process> processes;
    int currentActiveCicles = 0;
};

#endif // PROCESSLIST_H
